#!/bin/sh

# composer install if vendor not exists
if [ ! -d "/app/vendor/" ]; then
  cd /app/ && composer install && composer dump-autoload -a
fi

# log
chown -R www-data:www-data ./storage

# migrations
cd /app/ && php artisan migrate

# http
#php -S 0.0.0.0:80 -t /app
cd /app/ && php artisan serve --host=0.0.0.0 --port=80
