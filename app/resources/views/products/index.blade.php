@extends('layout')

@section('content')
    <!-- Main Content -->
    <main class="content">
        <div class="header-list-page">
            <h1 class="title">Products</h1>
            <a href="{{ route("products.create") }}" class="btn-action">Add new Product</a>
        </div>
        <table class="data-grid">
            <tr class="data-row">
                <th class="data-grid-th">
                    <span class="data-grid-cell-content">Name</span>
                </th>
                <th class="data-grid-th">
                    <span class="data-grid-cell-content">SKU</span>
                </th>
                <th class="data-grid-th">
                    <span class="data-grid-cell-content">Price</span>
                </th>
                <th class="data-grid-th">
                    <span class="data-grid-cell-content">Quantity</span>
                </th>
                <th class="data-grid-th">
                    <span class="data-grid-cell-content">Categories</span>
                </th>

                <th class="data-grid-th">
                    <span class="data-grid-cell-content">Actions</span>
                </th>
            </tr>
            @forelse ($products as $product)
                <tr class="data-row">
                    <td class="data-grid-td">
                        <span class="data-grid-cell-content">{{ $product->name }}</span>
                    </td>

                    <td class="data-grid-td">
                        <span class="data-grid-cell-content">{{ $product->sku }}</span>
                    </td>

                    <td class="data-grid-td">
                        <span class="data-grid-cell-content">R$ {{ number_format($product->price, 2, ',', '.') }}</span>
                    </td>

                    <td class="data-grid-td">
                        <span class="data-grid-cell-content">{{ $product->quantity }}</span>
                    </td>

                    <td class="data-grid-td">
                        <span class="data-grid-cell-content">
                            @if($product->categories)
                                {!! implode("<br>", $product->categories->pluck('name')->toArray()) !!}
                            @else
                                -
                            @endif
                        </span>
                    </td>

                    <td class="data-grid-td">
                        <div class="actions">
                            <div class="action edit"><span><a href="{{ route('products.edit', ['id' => $product->id]) }}">Edit</a></span></div>
                            <div class="action delete"><span><a href="{{ route('products.destroy', ['id' => $product->id]) }}">Delete</a></span></div>
                        </div>
                    </td>
                </tr>
            @empty
                <tr>
                    <td colspan="6">Nenhum produto cadastrado.</td>
                </tr>
            @endforelse
        </table>

        <br>
        {!! $products->render() !!}

    </main>
    <!-- Main Content -->
@endsection


