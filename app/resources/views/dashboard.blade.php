@extends('layout')

@section('content')

    <!-- Main Content -->
    <main class="content">
        <div class="header-list-page">
            <h1 class="title">Dashboard</h1>
        </div>
        <div class="infor">
            You have {{ $products->total() }} products added on this store: <a href="{{ route("products.index") }}" class="btn-action">Add new Product</a>
        </div>
        <ul class="product-list">
            @foreach($products as $product)
                <li>
                    <div class="product-image">
                        <img src="/assets/images/product/tenis-runner-bolt.png" layout="responsive" width="164" height="145" alt="{{ $product->name }}" />
                    </div>
                    <div class="product-info">
                        <div class="product-name"><span>{{ $product->name }}</span></div>
                        <div class="product-price"><span class="special-price">{{ $product->quantity }} available</span> <span>R$ {{ number_format($product->price, 2, ',', '.') }}</span></div>
                    </div>
                </li>
            @endforeach
        </ul>
    </main>
    <!-- Main Content -->
@endsection