<?php

namespace App\Http\Controllers;

use App\Models\ProductModel;
use App\Services\CategoryService;
use App\Services\ProductService;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    /** @var CategoryService */
    private $categoryService;
    
    /** @var ProductService */
    private $productService;

    /**
     * ProductController constructor.
     */
    public function __construct()
    {
        $this->categoryService = new CategoryService();
        $this->productService = new ProductService();
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        $products = $this->productService->getPaginate($request->input('per_page', 5));
        return view('products.index', ['products' => $products]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $categories = $this->categoryService->getAll();
        return view('products.form', [
            'product' => new ProductModel(),
            'categories' => $categories,
            'url' => route("products.store")
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->only(['sku', 'name', 'price', 'quantity', 'category', 'description']);

        $this->productService->validade($data);

        $product = $this->productService->create($data);

        return redirect()->route('products.edit', ['id'=>$product->id]);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $product = $this->productService->getById($id);
        $categories = $this->categoryService->getAll();
        return view('products.form', [
            'product' => $product,
            'categories' => $categories,
            'url' => route("products.update", ['id' => $product->id])
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->only(['sku', 'name', 'price', 'quantity', 'category', 'description']);
        $data['id'] = $id;

        $this->productService->validade($data);

        $product = $this->productService->edit($data);

        return redirect()->route('products.edit', ['id'=>$product->id]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $this->productService->delete($id);

        return redirect()->route('products.index');
    }
}
