<?php

namespace App\Services;

use App\Models\CategoryModel;
use Illuminate\Support\Facades\Validator;

/**
 * Class CategoryService
 * @package App\Services
 */
class CategoryService
{
    /**
     * @param string $id
     * @return CategoryModel|null
     */
    public function getById(string $id): ?CategoryModel
    {
        return CategoryModel::find($id);
    }


    /**
     * @param string $name
     * @return CategoryModel|null
     */
    public function getByName(string $name): ?CategoryModel
    {
        return CategoryModel::where('name', '=', $name)->first();
    }


    /**
     * @param int $perPage
     * @return mixed
     */
    public function getPaginate(int $perPage=25)
    {
        return CategoryModel::orderBy('name', 'asc')->paginate($perPage);
    }


    /**
     * @return mixed
     */
    public function getAll()
    {
        return CategoryModel::orderBy('name', 'asc')->get();
    }


    /**
     * @param array $data
     * @return CategoryModel
     */
    public function create(array $data): CategoryModel
    {
        return CategoryModel::create($data);
    }


    /**
     * @param string $name
     * @return mixed
     */
    public function findOrCreateByName(string $name)
    {
        return CategoryModel::firstOrCreate(['name' => $name]);
    }


    /**
     * @param array $data
     * @return CategoryModel
     */
    public function edit(array $data): CategoryModel
    {
        $category = $this->getById($data['id']);
        if(!$category)
            return null;

        $category->fill($data)->save();

        return $category;
    }


    /**
     * @param $id
     * @return int
     */
    public function delete($id): int
    {
        $category = CategoryModel::find($id);
        $category->products()->detach();
        return $category->delete();
    }

    /**
     * @param array $data
     */
    public function validade(array $data)
    {
        // valida
        // se a requisição for em json, retorna json
        // se veio de um blade, vai voltar p/ página antiga
        // e dentro da view $errors vai ficar disponível

        $id = $data['id'] ?? null;

        //dd($data); exit;

        Validator::make($data, [
            'name' => 'required|string|max:255',
            'code' => "required|string|unique:categories,code,{$id}|max:100",
        ])->validate();
    }
}