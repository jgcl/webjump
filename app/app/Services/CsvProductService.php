<?php

namespace App\Services;

/**
 * Class CsvProductService
 * @package App\Services
 */
class CsvProductService
{
    /**
     * @var ProductService
     */
    private $productService;


    /**
     * @var CategoryService
     */
    private $categoryService;


    /**
     * CsvProductService constructor.
     */
    public function __construct()
    {
        $this->productService = new ProductService();
        $this->categoryService = new CategoryService();
    }


    /**
     * Le o CSV e realiza ações de importação
     *
     * @param string $filePath
     */
    public function saveProductsFromCsv(string $filePath): void
    {
        // 1. carregar o conteúdo do arquivo
        $content = $this->getCsvFileContent($filePath);

        // 2. limpar cabeçalho
        $lines = explode("\n", $content);
        unset($lines[0]); // removendo o cabeçalho

        // 3. iterar produtos de csv para array/xml
        $products = $this->getProductsFromArrayLines($lines);

        // 4. salvar resultado
        $this->saveProductsAndCategories($products);

    }


    /**
     * Pega o conteúdo do CSV
     *
     * @param string $filePath
     * @return bool|string
     */
    public function getCsvFileContent(string $filePath)
    {
        return file_get_contents($filePath);
    }


    /**
     * Itera as linhas do CSV para um array de produtos e categorias
     *
     * @param array $lines
     * @return array
     */
    public function getProductsFromArrayLines(array $lines)
    {
        $products = [];
        foreach($lines as $line) {
            $parts = str_getcsv($line, ";");
            if(isset($parts[0])) {
                // formato a categoria

                // produto === nome;sku;descricao;quantidade;preco;categoria
                $products[] = [
                    'name'          => $parts[0],
                    'sku'           => $parts[1],
                    'description'   => $parts[2],
                    'quantity'      => $parts[3],
                    'price'         => $parts[4],
                    'categories'    => explode("|", $parts[5]),
                ];
            }

        }
        return $products;
    }


    /**
     * Salva produtos e categorias
     *
     * @param array $products
     */
    public function saveProductsAndCategories(array $products)
    {
        foreach($products as $productCandidate) {
            if(app()->runningInConsole())
                echo "sku {$productCandidate['sku']}\n";

            // pesquiso se já existe o sku, se não crio
            $exists = $this->productService->getBySku($productCandidate['sku']);

            // opa, já existe, pulo
            if($exists)
                continue;

            // nao existe, crio
            $product = $this->productService->create($productCandidate);

            // vinculo as categorias
            foreach($productCandidate['categories'] as $categoryName) {
                if(strlen($categoryName) == 0)
                    continue;

                $category = $this->categoryService->findOrCreateByName($categoryName);

                if(!$product->categories->where('id', '=', $category->id)->first())
                    $product->categories()->save($category);
            }
        }

    }
}