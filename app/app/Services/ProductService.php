<?php

namespace App\Services;

use App\Models\ProductModel;
use Illuminate\Support\Facades\Validator;

/**
 * Class ProductService
 * @package App\Services
 */
class ProductService
{
    /**
     * @param string $id
     * @return ProductModel|null
     */
    public function getById(string $id): ?ProductModel
    {
        return ProductModel::find($id);
    }

    /**
     * @param string $sku
     * @return ProductModel|null
     */
    public function getBySku(string $sku): ?ProductModel
    {
        return ProductModel::where('sku', '=', $sku)->first();
    }


    /**
     * @param int $perPage
     * @return mixed
     */
    public function getPaginate(int $perPage=25)
    {
        return ProductModel::orderBy('id', 'desc')->paginate($perPage);
    }


    /**
     * @param array $data
     * @return ProductModel
     */
    public function create(array $data): ProductModel
    {
        $product = ProductModel::create($data);
        $this->syncCategories($product, $data);
        return $product;
    }


    /**
     * @param ProductModel $product
     * @param array $data
     * @return ProductModel
     */
    private function syncCategories(ProductModel $product, array $data): ProductModel
    {
        if(isset($data['category']) && is_array($data['category']))
            $product->categories()->sync($data['category']);

        return $product;
    }


    /**
     * @param array $data
     * @return ProductModel|null
     */
    public function edit(array $data): ?ProductModel
    {
        $product = $this->getById($data['id']);
        if(!$product)
            return null;

        $product->fill($data)->save();

        $this->syncCategories($product, $data);

        return $product;
    }


    /**
     * @param $id
     * @return int
     */
    public function delete($id): int
    {
        $product = ProductModel::find($id);
        $product->categories()->detach();
        return $product->delete();
    }


    /**
     * @param array $data
     */
    public function validade(array $data)
    {
        // valida
        // se a requisição for em json, retorna json
        // se veio de um blade, vai voltar p/ página antiga
        // e dentro da view $errors vai ficar disponível

        $id = $data['id'] ?? null;

        //dd($data); exit;

        Validator::make($data, [
            'name' => 'required|string|max:255',
            'sku' => "required|string|unique:products,sku,{$id}|max:10",
            'description' => 'required|string',
            'quantity' => 'required|integer',
            'price' => 'required|numeric',
        ])->validate();
    }
}