<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Relations\BelongsToMany;

/**
 * Class CategoryModel
 * @package App\Models
 */
class CategoryModel extends \Illuminate\Database\Eloquent\Model
{
    protected $table = 'categories';
    public $timestamps = true;
    protected $dates = ['created_at', 'updated_at'];
    protected $fillable = [
        'name',
        'code',
    ];
    protected $casts = [
        'name' => "string",
        'code' => "string",
    ];

    /**
     * @return BelongsToMany
     */
    public function products(): BelongsToMany
    {
        return $this->belongsToMany(ProductModel::class, 'category_product', 'category_id', 'product_id');
    }

}