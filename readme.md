# Stack Adotada
- PHP 7.3
- Docker
- Docker Compose versão 3.2
- MySQL versão 5.7
- Redis 

# Subir utilizando modo rápido 1
- Clique e veja a aplicação rodando no Play With Docker.

[![](https://raw.githubusercontent.com/play-with-docker/stacks/cff22438cb4195ace27f9b15784bbb497047afa7/assets/images/button.png)](http://play-with-docker.com/?stack=https://bitbucket.org/jgcl/webjump/raw/2813d860591e16839bc54640821869b8a0216b31/docker-compose.yml "Test Online in play with docker")

# Subir utilizando modo rápido 2
- Com o Docker instalado:
- `git clone https://jgcl@bitbucket.org/jgcl/webjump.git`
- `docker pull jgcl88/webjump`
- `docker-compose -f docker-compose-no-volumes.yml up -d`
- `docker exec -it app.webjump php artisan migrate`

# Subir realizando build da imagem
- Com o Docker instalado:
- `git clone https://jgcl@bitbucket.org/jgcl/webjump.git`
- `docker-compose build`
- `docker-compose up -d`
- `docker exec -it app.webjump php artisan migrate`

# Como importar o CSV de produtos
- `docker exec -it app.webjump php artisan webjump:importcsv`

# Solução
- Trabalhei no modelo MVCS (Model View Controller Service). 

No universo Laravel, para simples localização dentro do framework, seguir esses passos:
- Rota > Controler > Serviço > Banco de Dados;
- Comando > Serviço > Banco de Dados;

# Minhas limitações de tempo
- Adotar o modelo MVCSR (Model View Controler Service Repository);
- Documentação/Swagger dos endpoints;
- Endpoints compatível com json (na verdade já são porém todo o retorno está em HTML); 
- Eliminar o Laravel 5.8 e utilizar componentes soltos (por exemplo componentes do Symfony);
- Implementar o upload de imagens (é fácil usando as facades do Laravel, porém estou sem tempo mesmo);
- Implementar o log de mudanças;

# Autor
João Gabriel Casteluber Laass
gabriel@joaogabriel.org
27 9 8802-0195


